import { Component, Input } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';

/**
 * The login component
 */
@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})

export class LoginComponent {
    /**
     * The entry type from the parent componetns
     */
    @Input() type: string;

    /**
     * Local reference of loginForm group
     */
    loginForm: FormGroup;


    constructor(private _fb: FormBuilder) {
        this.loginForm = _fb.group({
            name: '',
            password: ''
        });
    }

    /**
     * Login user method
     * 
     * @param formValue value of loginForm
     */
    login(formValue): void {
        console.log(formValue);
    }
}
