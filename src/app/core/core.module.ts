import { NgModule, Optional, SkipSelf } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { SharedModule } from '../shared/shared.module';
import { LoginModule } from '../login/login.module';
import { UsersModule } from '../users/users.module';

// import { SomeSingletonService } from './some-singleton/some-singleton.service';

@NgModule({
    imports: [
        CommonModule,
        HttpClientModule,
        SharedModule,
        LoginModule,
        UsersModule
    ],
    declarations: [],
    providers: [
        /* our own custom services  */
        // SomeSingletonService
    ]
})

export class CoreModule {
    /**
     * make sure CoreModule is imported only by one NgModule the AppModule
     * 
     * @param parentModule 
     */
    constructor(
        @Optional() @SkipSelf() parentModule: CoreModule
    ) {
        if (parentModule) {
            throw new Error('CoreModule is already loaded. Import only in AppModule');
        }
    }
}