import { Component, Input } from '@angular/core';

/**
 * The button component
 * 
 * Shared styled button for all components
 */
@Component({
    selector: 'app-button',
    templateUrl: './button.component.html',
    styleUrls: ['./button.component.scss']
})

export class ButtonComponent {
    /**
     * The entry type from the parent componetns
     */
    @Input() type: string;


    /**
     * OnClick button callback
     * 
     * @param {event} target The target that returns stardard click event
     */
    onButtonClick(event: Event):void {
        console.log(event);
    }
}
