import { Component } from '@angular/core';

/**
 * The users list component
 */
@Component({
    selector: 'app-user',
    templateUrl: './users.component.html',
    styleUrls: ['./users.component.scss']
})

export class UsersComponent {

    /** 
     * Example of user's list
     */
    usersList = [
        "user1",
        "user2",
        "user3"
    ]

}
